import socketserver
import binascii
import socket
import struct
import sys
import datetime
import os
import board
from dotenv import load_dotenv
import mysql.connector
from mysql.connector import Error
from DB_Connection import connection,host,user,password,database
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DateTime,Text,BLOB,Enum,update,text
from sqlalchemy.dialects.mysql.dml import Insert
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker

env_path = 'config.env'
load_dotenv(dotenv_path=env_path)


class _senseon_ :
    Meta = MetaData()
    audit_trail = Table(
        'audit_trail', Meta,
        Column('Controller_UID',Text), 
        Column('Audit', Text),
        Column('Audit_Address',Text),
        )

class _Senseon_NG_ :
    Meta = MetaData()
    network_configuration = Table(
        'network_configuration', Meta,
        Column('name', Enum("network","network_temp"),nullable=False,primary_key=True), 
        Column('config', Text),
        Column('dt_stamp', DateTime, default=datetime.datetime.utcnow),
        )



class TCP_ServerHandler(socketserver.BaseRequestHandler):


    def handle(self):
 
        self.data = self.request.recv(1024).strip()
        print("{} wrote:".format(self.client_address[0]))


        if self.data.decode('utf-8')[0] == '1':

            heartbeat = self.data
            decoded_data = board.decodeStatus(heartbeat)
            
            check = False
            hexList = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']        
            if (decoded_data['status'][2] in hexList) and (decoded_data['status'][3] in hexList):
                check = True
            if (decoded_data['status'][:2] == "04") and (check):
                board.setStatus(decoded_data)

            engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
            cursor = engine.connect()
            cursor.execute("INSERT INTO `senseon`.`heartbeat` (`heartbeat`) VALUES (%(heartbeat)s);",
                           {'heartbeat': heartbeat})

            # return_data = "04default"
            return_data = "04default"
            command_queue = board.getQueuedCommand(decoded_data)
            # print('command_queue',type(command_queue))
            if len(command_queue) > 0:
                return_data = command_queue["command"]
                print(type(return_data))
                board.exeQueuedCommand(command_queue)
                return self.request.sendall(return_data)
            # return_data["id"] = cursor.lastrowid
            # just send back the same data, but upper-cased
            print('\nreturn_data =',return_data,'\n')
            return self.request.sendall(str.encode(return_data))

        elif self.data.decode('utf-8')[0] != '%':

            Controller_UID = self.data.decode('utf-8').split('AU')[0][2:]
            Audit = self.data.decode('utf-8').split('AU')[1].split('@')[0]
            Audit_Address = self.data.decode('utf-8').split('AU')[1].split('@')[1]
            print('\nController_UID     ',Controller_UID)
            print('Audit              ',Audit)
            print('Audit_Address      ',Audit_Address,'\n')
            engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
            query = Insert(_senseon_.audit_trail).values(Controller_UID=Controller_UID,Audit = Audit, Audit_Address= Audit_Address)
            conn = engine.connect()
            result = conn.execute(query,{'Controller_UID':Controller_UID ,'Audit':Audit,'Audit_Address':Audit_Address})
            return_data = f'{Audit_Address}\n'
            data = b'1A'+ return_data.encode()
            self.request.sendall(data)

        else:

            engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
            s=select([_Senseon_NG_.network_configuration]).where(_Senseon_NG_.network_configuration.c.name=='network_temp')
            result = engine.execute(s)
            cust = [data for data in result] 
            eval_data1 = [json_data[1] for json_data in cust]
            if eval_data1 != []:
                eval_data = [eval(eval_data1[0])]
                return_data = f'\n\n%WIFI\n{eval_data[0]["ssid"]}\n{eval_data[0]["password"]}\n%END\n'
                print(return_data)
                self.request.sendall(return_data.encode())
            else:
                pass


if __name__ == "__main__":
    HOST, PORT = "localhost", 9999

    with socketserver.TCPServer((HOST, PORT), TCP_ServerHandler) as server:
        server.serve_forever()
