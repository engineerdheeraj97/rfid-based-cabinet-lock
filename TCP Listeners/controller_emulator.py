import socket
import sys
import time
import binascii
import util

boards = [
    {'status': '04001', 'sendStatus': '04001', 'boardNum': '1', 'lockStatus': '0110001100110101', 'alarmStatus': True},
    {'status': '04c50', 'sendStatus': '04000', 'boardNum': '2', 'lockStatus': '0110001100110101', 'alarmStatus': False}]

delay = 5


class Controller_emulator():

    def setInterval():
    
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('127.0.0.1', 9999)
        sock.connect(server_address)
        try:
            itr = 0
            sock.sendall(str.encode(str(itr+1)+boards[itr]['status']))

            boards[itr]['sendStatus'] = boards[itr]['sendStatus'][:2] + '00' + boards[itr]['sendStatus'][4:]

            amount_received = 0
            amount_expected = 9
            message_received = b''

            while amount_received < amount_expected:
                data = sock.recv(1024)
                if len(data) > 0:
                    if str(data)[2] == 'H':
                        amount_expected = 9
                        message_received = b''
                        amount_received = 0
                    elif str(data)[2] == 'O':
                        amount_expected = 2
                        message_received = b''
                        amount_received = 0
                    elif str(data)[2:-1] == '04default':
                        break
                else:
                    break
                message_received += data
                amount_received += len(data)
            print('\nreceived return data   ', data)
            if len(message_received)>0:
                if str(message_received)[2] == 'H':
                    # Todo Simulate card add
                    print()
                elif str(message_received)[2] == 'O':
                    # lock_toggles = util.toBinary(message_received[1], length=8)
                    lock_toggles = str(bin(message_received[1]))[2:]
                    lock_toggles = util.padding(input=lock_toggles, length=8)
                    prev_status = util.hex_to_binary(boards[itr]['status'][2:4])
                    next_status = ''
                    send_status = ''
                    for i, r in enumerate(range(7, 3, -1)):
                        if lock_toggles[r] == '1':
                            status = ('1' if prev_status[2 * i] == '0' else '0')
                            status += prev_status[(2 * i) + 1]
                            send_status += status
                            next_status += status
                        if lock_toggles[r] == '0':
                            send_status += '00'
                            next_status += prev_status[2 * i:(2 * i) + 2]
                    boards[itr]['status'] = boards[itr]['status'][:2] + util.binary_to_hex(next_status) + boards[itr]['status'][4:]
                    boards[itr]['sendStatus'] = boards[itr]['status'][:2] + util.binary_to_hex(send_status) + boards[itr]['status'][4:]
                    # print >> sys.stderr, 'received "%s"' % data

        finally:
            print('\nclosing socket ')
            # print >> sys.stderr, 'closing socket'
            sock.close()

    def network_provosion_Handler():

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('127.0.0.1', 9999)
        sock.connect(server_address)
        sock.sendall(b'%P5410ecfe1aa5')
        data = sock.recv(1024)
        print('\nReceived Provisioning  ',data)
        sock.close()

    def audit_trail_Handler():

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('127.0.0.1', 9999)
        sock.connect(server_address)
        sock.sendall(b'SN5410ecfe1aa5AU130101123739001001017b050505c003@0000A0')
        data = sock.recv(1024)
        print('\nReceived audit_trail   ',data)
        sock.close()

Controller_emulator.audit_trail_Handler()
Controller_emulator.network_provosion_Handler()
while True:

    Controller_emulator.setInterval()
    time.sleep(delay)