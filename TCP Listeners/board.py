from DB_Connection import connection,host,user,password,database
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DateTime,Text,BLOB,Enum,update,text
from sqlalchemy.dialects.mysql.dml import Insert
import DB_Connection
import datetime
import json
import util
import math
import os

# class _senseon_status_ :
#     Meta = MetaData()
#     status = Table(
#         'status', Meta,
#         Column('boardNum', Enum("network","network_temp"),nullable=False,primary_key=True), 
#         Column('status', Text),
#         Column('dt_stamp', DateTime, default=datetime.datetime.utcnow),
#         )

def decodeStatus(heartbeat):
    data = {}
    heartbeat = heartbeat.decode("utf-8")
    data["boardNum"] = heartbeat[0]
    data["status"] = heartbeat[1:]
    return data


def setStatus(data):
    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    cursor = engine.connect()
    cursor.execute(
        "INSERT INTO `senseon`.`status` (`boardNum`, `status`) VALUES (%(boardNum)s, %(status)s) ON DUPLICATE KEY UPDATE `status` = %(status)s, `locks_triggered` = '[]', `dt_stamp` = CURRENT_TIMESTAMP ;",
        {'boardNum': data["boardNum"], 'status': data['status']})
    # boardNum = data["boardNum"]
    # status = data['status']
    # query = Insert(_senseon_status_.status).values(boardNum=boardNum,status=status)
    # query = query.on_duplicate_key_update(data=query.inserted.boardNum,status=status)
    # conn = engine.connect()
    # result = conn.execute(query,{'boardNum':data["boardNum"],'status':data['status']})
    return


def queueCommand(data):
    return_data = {}
    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    cursor = engine.connect()
    cursor.execute(
        "INSERT INTO `senseon`.`commands_queue` (`boardNum`, `command_blob`) VALUES (%(boardNum)s, %(command)s);",
        {'boardNum': data["boardNum"], 'command': data["command"]})
    return_data["id"] = cursor.lastrowid
    return return_data


def getQueuedCommand(data):
    print('\ndata = ',data)
    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    cursor = engine.connect()
    result = cursor.execute(
        "SELECT `idcommands_queue`, `boardNum`, `command_blob` as `command`, `status` FROM senseon.commands_queue WHERE boardNum = %(boardNum)s AND status = 'queued' ORDER BY dt_stamp ASC LIMIT 1",
        {'boardNum': data['boardNum']})
    commands=[]
    for command in result:
        commands.append(command)
    return commands


def exeQueuedCommand(data):
    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    cursor = engine.connect()
    cursor.execute(
        "UPDATE `senseon`.`commands_queue` SET `status` = 'executed' WHERE (`idcommands_queue` = '%(idcommands_queue)s');",
        {'idcommands_queue': data["idcommands_queue"]})
    return


def getStatus():
    # data = {}
    boards = execute_read_query(
        "SELECT boardNum, status, unix_timestamp(dt_stamp) dt_stamp, IF(TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP,dt_stamp))<5, TRUE, FALSE) AS online_status FROM senseon.status;",
        dictionary=True)
    return boards
