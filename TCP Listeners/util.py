import os
import random
import string
# import db
# import cv2
import binascii
import math
import logging

# logging.basicConfig(filename=r'.\logs\main.log', level=logging.DEBUG)


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


# def get_image_from_fielId(fileId):
#     forms_uploads_dir = r'.\forms\uploads'
#     select_q = db.execute_read_query("SELECT * FROM dais.form_file WHERE id=%(fileId)s", {'fileId': fileId})
#     form_path = os.path.join(forms_uploads_dir, select_q[0][1])
#     image = cv2.imread(form_path)
#     # image = cv2.cvtColor(cv2.imread(form_path), cv2.COLOR_BGR2GRAY)
#     return image


# def get_image_from_uri(uri):
#     forms_uploads_dir = r'.\forms\uploads'
#     form_path = os.path.join(forms_uploads_dir, uri)
#     image = cv2.imread(form_path)
#     return image


def utf_16_to_8_bits(data):
    decoded = data.encode("utf-16be")
    hexified = binascii.hexlify(decoded)
    # hexified = hexified[2:]
    return_data = b''
    # for idx in range(len(hexified)):
    #     return_data = return_data+hexified[idx:idx+1]
    # return_data = '0b'
    for idx in range(len(hexified)):
        if math.floor(idx / 2) % 2 != 0:
            return_data = return_data + hexified[idx:idx + 1]
    return binascii.unhexlify(return_data)


def hex_to_binary(hex_code_str, length=8):
    hex_code = int(hex_code_str, 16)
    bin_code = bin(hex_code)[2:]
    padding = (length - len(bin_code) % length) % length
    return '0' * padding + bin_code

def padding(input, length, char='0'):
    pad = (length - len(input) % length) % length
    return char * pad + input

def binary_to_hex(binary_code_str, length=2):
    binary_code = int(binary_code_str, 2)
    dec_str = format(binary_code, 'x')
    padding = (length - len(dec_str) % length) % length
    return '0' * padding + dec_str

def toBinary(byte_str, length=8):
    msg = ''.join(format(ord(i), 'b') for i in byte_str)
    padding = (length - len(msg) % length) % length
    return '0' * padding + msg

# def log(Message=None):
#     return logging
# print(hex_to_binary("01"))
# print(binary_to_hex("00001101"))