from flask import Flask,request,jsonify,session,make_response,Response
from DB_Connection import connection,host,user,password,database
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DateTime,Text,BLOB,Enum,update,text
from sqlalchemy.dialects.mysql.dml import Insert
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker
from flask_httpauth import HTTPBasicAuth
from dotenv import load_dotenv
import validator
import re
import hashlib
import pandas as pd
import os
import string
import random
import json

app = Flask(__name__)
auth = HTTPBasicAuth()


env_path = 'config.env'
load_dotenv(dotenv_path=env_path)

# env_path1 = 'salt.env'
# load_dotenv(dotenv_path=env_path1)

token_size = os.environ.get("token_size")

class __senseon__UserSession1__ :
    Meta = MetaData()
    UserSession = Table(
        'UserSession', Meta,
        Column('UserID', Integer,primary_key=True), 
        Column('Token', String),
        Column('User_Agent', String),
    )

class __senseon__User__ :
    Meta = MetaData()
    User1 = Table(
        'User1', Meta,
        Column('UserID', Integer,primary_key=True), 
        Column('Username', String),
        Column('Pass', String),
        Column('FirstName', String),
        Column('LastName', String),
    )

@auth.verify_password
def verify(username,passw):

	# write_random_key(env_path[0])
	# print(get_config_salt())
	password = hashlib.pbkdf2_hmac('sha512', bytes(passw,'utf-8'), bytes(os.environ.get("salt"),'utf-8'),int(os.environ.get("iterations"))).hex()
	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	connection = engine.connect()
	sql_q = 'select UserID from User1 where Username=%(user)s and Pass=%(pass)s' 
	val={'user':username,'pass':password}
	# print(passw)
	result =connection.execute(sql_q,val)
	if result:
		for x in result:
			for i in x:
				return i
	else:
		return False


@auth.login_required
def login_valdidate(username,passw):

	# write_random_key(env_path[0])
	passw = hashlib.pbkdf2_hmac('sha512', bytes(passw,'utf-8'), bytes(os.environ.get("salt"),'utf-8'),int(os.environ.get("iterations"))).hex()
	print('login_valdidate',passw)
	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	DBSession = sessionmaker(bind=engine)
	session = DBSession()
	session.query(__senseon__User__.User1).filter_by(Username=username,Pass=passw).first()[0]


@auth.error_handler
def auth_error():
	try:
		raise validator.InvalidAuthentication()
	except validator.InvalidAuthentication as InvalidAuthenticationError:
		if validator.verify_ErrorReason('validator.InvalidAuthentication') == validator.get_ErrorCode('validator.InvalidAuthentication'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidAuthentication", 'error_code' : validator.get_ErrorCode('validator.InvalidAuthentication')}))
			response.mimetype = 'application/json'
			return response

def verify_token(token):
	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	connection = engine.connect()
	query='select * from UserSession where token=%(token)s'
	val={'token':token}
	result=connection.execute(query,val)
	for row in result:
		if(row):
			return True
		else:
			return False

def insert_val(UserID,token_pass,User_Agent):

    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    conn = engine.connect()
    ins = Insert(__senseon__UserSession1__.UserSession).values(UserID=UserID,Token=token_pass,User_Agent=User_Agent)
    conn = engine.connect()
    result = conn.execute(ins,{'UserID':UserID,'Token':token_pass,'User_Agent':User_Agent})


def get_userID(token):

	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	connection = engine.connect()
	query='select UserID from UserSession where token=%(token)s'
	val={'token':token}
	result=connection.execute(query,val)
	if result:
		for row in result:
			for user_id in row:
				return user_id
	else:
		return False

def verify_Username(Username):

    token = request.headers.get('token') 
    engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
    connection = engine.connect()
    query='select * from User1 where Username=%(Username)s'
    val={'Username':Username}
    result=connection.execute(query,val)
    for row in result:
    	print(row[1])
    	if(row[1]):
    		raise validator.InvalidUsername()
    	else:
    		return True


@auth.login_required
def login_func():
	token = random_token()
	user_agent = request.headers.get('User-Agent')
	insert_val(auth.current_user(),token,user_agent)
	response = Response(json.dumps({'token' : token,'status': 'success', 'error_code' : ''}))
	response.mimetype = 'application/json'
	return response

def random_token(size=75, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def logout_func():
	Token = request.headers.get('token')
	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	connection = engine.connect()
	query = "delete from UserSession where UserID =%(UserID)s and Token =%(Token)s"
	val={'UserID':get_userID(Token),'Token':Token}
	user_id = get_userID(Token)
	connection.execute(query,val)
	response = Response(json.dumps({"status":"successful","message":"UserID: {} current session deleted in usersession table".format(user_id)}))
	response.mimetype = 'application/json'
	return response

def update_reset_usersession(User_ID):
	Token = request.headers.get('token')
	engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
	connection = engine.connect()
	query = "delete from UserSession where UserID =%(UserID)s"
	print(User_ID)
	val={'UserID':User_ID}
	connection.execute(query,val)
	return {"status":"successful","message":"UserID: {} login details deleted in usersession table".format(auth.current_user())}
