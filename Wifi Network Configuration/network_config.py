from flask import Flask,request,jsonify,session,make_response,Response
from DB_Connection import connection,host,user,password,database
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DateTime,Text,BLOB,Enum,update,text,insert
from sqlalchemy.dialects.mysql.dml import Insert
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker
import jsonschema
from jsonschema import validate
from dotenv import load_dotenv
from flask_cors import CORS
import hashlib
import collections
import validator
import authenticator
import sqlalchemy
import os
import datetime
import json
import string
import time
import random
import pandas as pd


app = Flask(__name__)
CORS(app)


env_path = 'config.env'
load_dotenv(dotenv_path=env_path)


class _senseon_ :
	Meta = MetaData()
	network_configuration = Table(
		'network_configuration', Meta,
		Column('name', Enum("network","network_temp"),nullable=False,primary_key=True), 
		Column('config', Text),
		Column('dt_stamp', DateTime, default=datetime.datetime.utcnow),
		)

class __senseon__User__ :
    Meta = MetaData()
    User1 = Table(
        'User1', Meta,
        Column('UserID', Integer,primary_key=True), 
        Column('Username', String),
        Column('Pass', String),
    )

class JSONSConfigchema:
	schema = {
	    "type" : "object",
	    "properties" : {
	        "ssid" : {"type" : "string","minLength": 5,"maxLength": 12},
	        "security" : {"type" : "string","minLength": 3,"maxLength": 5},
	        "password" : {"type" : "string"},
	    },
	    "required": ["ssid", "security", "password"]
	}

# class JSONSUserchema:
# 	schema = {
# 	    "type" : "object",
# 	    "properties" : {
# 	        "usern" : {"type" : "string","minLength": 5,"maxLength": 10},
# 	        "passw" : {"type" : "string","minLength": 5,"maxLength": 10},
# 	    },
# 	    "required": ["usern", "passw"]
# 	}

@app.route('/',methods=['GET'])
def Access_API():

	try:

		raise validator.PageNotFoundError()

	except validator.PageNotFoundError as PageNotFoundError:
		print(validator.get_ErrorCode('validator.PageNotFoundError'))
		if validator.verify_ErrorReason('validator.PageNotFoundError') == validator.get_ErrorCode('validator.PageNotFoundError'):
			response = Response(json.dumps({"status":"failed","message":"validator.PageNotFoundError", 'error_code' : validator.get_ErrorCode('validator.PageNotFoundError')}))
			response.mimetype = 'application/json'
			return response

@app.route('/gac/auth/login',methods=['GET','POST'])
def login_function():
	try:

		if request.method == 'POST':
			log_in =authenticator.login_func()
			return log_in
		else:
			raise validator.InvalidMethodRequest()

	except validator.InvalidMethodRequest as InvalidMethodRequest:
		if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
			response.mimetype = 'application/json'
			return response

@app.route('/gac/auth/logout',methods=['GET','POST'])
def logout_function():

	try:

		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:

				if request.method == 'POST':

					log_out = authenticator.logout_func()
					return log_out
				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()

	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response

@app.route('/gac/network/get',methods=['GET','POST'])
@app.errorhandler(502)
def get_network_config():

	try:
		start = time.time()
		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:
				if request.method == 'GET':

					engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
					s=select([_senseon_.network_configuration]).where(_senseon_.network_configuration.c.name=='network_temp')
					result = engine.execute(s)
					cust = [data for data in result] 
					eval_data1 = [json_data[1] for json_data in cust]
					eval_data = [eval(eval_data1[0])] 
					config = {'ssid': eval_data[0]['ssid'],'security': eval_data[0]['security'],'password':eval_data[0]['password'],'network_test':eval_data[0]['network_test']}
					response = {'response':config,'status': 'success', 'error_code' : ''}
					end = time.time()
					print(f"Runtime of the program is {end - start}")
					response = Response(json.dumps(response))
					response.mimetype = 'application/json'
					return response
				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()

	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response


@app.route('/gac/network/config', methods=['GET', 'POST'])
@app.errorhandler(502)
def post_network_config():

	try:
		start = time.time()
		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:
				if request.method == 'POST':

					try:

						json_dict = request.get_json()
						ssid = json_dict['ssid']
						security = json_dict['security']
						password = json_dict['password']
						validator.Valid_Security(security)
						validator.check_strogness(password)
						config_id = random_key()
						config = {'ssid': ssid,'security':security,'password':password,'config_id':config_id,'network_test':'none'}
						validate(instance=config, schema=JSONSConfigchema.schema)
						config1 = str(config)
						# print(config)
						name ='network_temp'
						engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
						query = Insert(_senseon_.network_configuration).values(name=name,config=config1)
						query = query.on_duplicate_key_update(data=query.inserted.name,config=config1)
						conn = engine.connect()
						result = conn.execute(query,{'name':name,'config':config1})
						response = {'response':config,'status': 'success', 'error_code' : ''}
					
					except jsonschema.exceptions.ValidationError as jsonschema_ValidationError:
						if validator.verify_ErrorReason('jsonschema.exceptions.ValidationError') == validator.get_ErrorCode('jsonschema.exceptions.ValidationError'):
							response = Response(json.dumps({"status":"error","message":"jsonschema.exceptions.ValidationError", 'error_code' : validator.get_ErrorCode('jsonschema.exceptions.ValidationError')}))
							response.mimetype = 'application/json'
							return response
					
					except validator.InvalidPasswordFormatError as InvalidPasswordFormatError_err:
						if validator.verify_ErrorReason('validator.InvalidPasswordFormatError') == validator.get_ErrorCode('validator.InvalidPasswordFormatError'):
							response = Response(json.dumps({"status":"error","message":"validator.InvalidPasswordFormatError ", 'error_code' : validator.get_ErrorCode('validator.InvalidPasswordFormatError')}))
							response.mimetype = 'application/json'
							return response
					
					except validator.InvalidSecurityNameError as InvalidSecurityNameError_err:
						if validator.verify_ErrorReason('validator.InvalidSecurityNameError') == validator.get_ErrorCode('validator.InvalidSecurityNameError'):
							response = Response(json.dumps({"status":"error","message":"validator.InvalidSecurityNameError", 'error_code' : validator.get_ErrorCode('validator.InvalidSecurityNameError')}))
							response.mimetype = 'application/json'
							return response

					end = time.time()
					print(f"Runtime of the program is {end - start}")
					response = Response(json.dumps(response))
					response.mimetype = 'application/json'
					return response

				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()

	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response
	

@app.route('/gac/network/activate', methods=['GET', 'POST'])
def activate_network_config():

	try:
		start = time.time()
		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:
				if request.method == 'POST':

					engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(user,password,host,database))
					s=select([_senseon_.network_configuration]).where(_senseon_.network_configuration.c.name=='network_temp')
					result = engine.execute(s)
					cust = [data for data in result] 
					eval_data1 = [json_data[1] for json_data in cust]
					eval_data = [eval(eval_data1[0])]
					print(eval_data)
					True_record = []
					for dict_data in eval_data:
						if dict_data['network_test'] ==  'True':
							True_record.append(dict_data)
						else:
							True_record.append(dict_data)
							return {"status":"failed","message":"network_test id not true","response":json.loads(json.dumps(True_record))}
					name = 'network'
					if name == 'network':
						config = True_record
						config = config[0]
						del config['config_id']
						config1 = str(config)
						query = Insert(_senseon_.network_configuration).values(name=name,config=config1)
						query = query.on_duplicate_key_update(data=query.inserted.name,config=config1)
						conn = engine.connect()
						result = conn.execute(query,{'name':name,'config':config1})
						response = {'response':config,'status': 'success', 'error_code' : ''}
						end = time.time()
						print(f"Runtime of the program is {end - start}")
						response = Response(json.dumps(response))
						response.mimetype = 'application/json'
						return response
				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()

	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response


@app.route('/gac/network/test', methods=['GET', 'POST'])
def test_network_config():

	try:
		start = time.time()
		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:

				if request.method == 'POST':

					engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(user,password,host,database))
					s=select([_senseon_.network_configuration]).where(_senseon_.network_configuration.c.name=='network_temp')
					result = engine.execute(s)
					cust = [data for data in result] 
					eval_data1 = [json_data[1] for json_data in cust]
					eval_data = [eval(eval_data1[0])] 
					config = {'ssid': eval_data[0]['ssid'],'security': eval_data[0]['security'],'password':eval_data[0]['password'],'network_test':eval_data[0]['network_test'],'config_id':eval_data[0]['config_id']}
					print(config)
					name = 'network_temp'
					if True:
						config['network_test'] = 'True'
						config1 = str(config)
						stmt=_senseon_.network_configuration.update().where(\
		                    _senseon_.network_configuration.c.name==name).values(config=config1)
						conn = engine.connect()
						conn.execute(stmt)
					else:
						config['network_test'] = 'False'
						config1 = str(config)
						stmt=_senseon_.network_configuration.update().where(\
		                    _senseon_.network_configuration.c.name==name).values(config=config1)
						conn = engine.connect()
						conn.execute(stmt)
					response = {'response':config,'status': 'success', 'error_code' : ''}
					end = time.time()
					print(f"Runtime of the program is {end - start}")
					response = Response(json.dumps(response))
					response.mimetype = 'application/json'
					return response
				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()

	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response

# {"usern":"Dheeraj97","passw":"$Dais@123$"}
@app.route('/gac/auth/update',methods=['GET','POST'])
def login_update_function():

	try:

		start = time.time()
		token=request.headers.get('token')
		if authenticator.verify_token(token):

			try:

				if request.method == 'POST':

					try:

						json_dict = request.get_json()
						usern = json_dict['usern']
						passw = json_dict['passw']
						config = {'usern': usern,'passw':passw}
						# validate(instance=config, schema=JSONSUserchema.schema)
						validator.Valid_Username(usern)
						# validator.check_strogness(passw)
						# password = hashlib.sha512(passw.encode()).hexdigest()
						password = hashlib.pbkdf2_hmac('sha512', bytes(passw,'utf-8'), bytes(os.environ.get("salt"),'utf-8'),int(os.environ.get("iterations"))).hex()
						usern = str(usern)
						password = str(password)

						try:

							if authenticator.verify_Username(usern):
								pass
							else:
								engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(os.environ.get("user"),os.environ.get("password"),os.environ.get("host"),os.environ.get("database")))
								User_ID = authenticator.get_userID(token)
								print('User_ID',User_ID)
								stmt=__senseon__User__.User1.update().where(\
									__senseon__User__.User1.c.UserID==User_ID).values(Username=usern,Pass=password)
								conn = engine.connect()
								conn.execute(stmt,{'UserID':User_ID,'Username':usern,'Pass':password})
							end = time.time()
							print(f"Runtime of the program is {end - start}")
						
						except validator.InvalidUsername as InvalidUsername_err:
							if validator.verify_ErrorReason('validator.InvalidUsername') == validator.get_ErrorCode('validator.InvalidUsername'):
								response = Response(json.dumps({"status":"error","message":"validator.InvalidUsername", 'error_code' : validator.get_ErrorCode('validator.InvalidUsername')}))
								response.mimetype = 'application/json'
								return response
					
					except validator.InvalidUsernameFormatError as InvalidUsernameFormatError_err:
						if validator.verify_ErrorReason('validator.InvalidUsernameFormatError') == validator.get_ErrorCode('validator.InvalidUsernameFormatError'):
							response = Response(json.dumps({"status":"error","message":"validator.InvalidUsernameFormatError", 'error_code' : validator.get_ErrorCode('validator.InvalidUsernameFormatError')}))
							response.mimetype = 'application/json'
							return response

					except validator.InvalidPasswordFormatError as InvalidPasswordFormatError_err:
						if validator.verify_ErrorReason('validator.InvalidPasswordFormatError') == validator.get_ErrorCode('validator.InvalidPasswordFormatError'):
							response = Response(json.dumps({"status":"error","message":"validator.InvalidPasswordFormatError ", 'error_code' : validator.get_ErrorCode('validator.InvalidPasswordFormatError')}))
							response.mimetype = 'application/json'
							return response

					except jsonschema.exceptions.ValidationError as jsonschema_ValidationError:
						if validator.verify_ErrorReason('jsonschema.exceptions.ValidationError') == validator.get_ErrorCode('jsonschema.exceptions.ValidationError'):
							response = Response(json.dumps({"status":"error","message":"jsonschema.exceptions.ValidationError", 'error_code' : validator.get_ErrorCode('jsonschema.exceptions.ValidationError')}))
							response.mimetype = 'application/json'
							return response

					authenticator.update_reset_usersession(User_ID)	
					response = Response(json.dumps({'status': 'success', 'error_code' : ''}))
					response.mimetype = 'application/json'
					return response

				else:
					raise validator.InvalidMethodRequest()

			except validator.InvalidMethodRequest as InvalidMethodRequest:
				if validator.verify_ErrorReason('validator.InvalidMethodRequest') == validator.get_ErrorCode('validator.InvalidMethodRequest'):
					response = Response(json.dumps({"status":"failed","message":"validator.InvalidMethodRequest", 'error_code' : validator.get_ErrorCode('validator.InvalidMethodRequest')}))
					response.mimetype = 'application/json'
					return response
		else:
			raise validator.InvalidToken()
			
	except validator.InvalidToken as InvalidTokenError:
		if validator.verify_ErrorReason('validator.InvalidToken') == validator.get_ErrorCode('validator.InvalidToken'):
			response = Response(json.dumps({"status":"failed","message":"validator.InvalidToken", 'error_code' : validator.get_ErrorCode('validator.InvalidToken')}))
			response.mimetype = 'application/json'
			return response


def random_key(size=10, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))



if __name__ == '__main__':
	app.run(port=6000)