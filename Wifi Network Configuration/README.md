# Wifi Network Configuration Module

## MicroSrvice Created for Wifi Configguration with Flask, ORM, AWS
➊  Created Table for Network Configuration with required schema : Backend tables in RDS
➋ Created Network API  with FLASK
                      ✪  Created the Login Method with token based authentication and tested with basic authorization (username and password).                              Password encryption has been done              

                           with SHA512  HASH encryption algorithm

                      ✪  Created config method to configure WIFI network in the Raspberry Pie and Validate the inputs with specific 
                           schema and raise custom errors to handle it.

                        

                      ✪  Created  a Test method to check whether the temporary network is ready to activate or not.

                      ✪  Created a Activate method to activate the temporary network to a standard network

                      ✪  Created a logout method to delete the token details of a particular session of a login user.

                      ✪  Created an update method to modify the username and password and delete all the existing 
                           token details of the particular login user.
➌  Implemented custom exception handler with specific error codes to handle unexpected exceptions in the Network API.
