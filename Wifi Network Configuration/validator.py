from flask import Flask,request,jsonify,session,make_response,Response
from DB_Connection import connection,host,user,password,database
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,String,DateTime,Text,BLOB,Enum,update,text
from sqlalchemy.dialects.mysql.dml import Insert
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker
from flask_httpauth import HTTPBasicAuth
from dotenv import load_dotenv
import re
import hashlib
import pandas as pd
import os
import string
import random
import json

app = Flask(__name__)


class Error_Table:

	Data = Error_Table =  {'jsonschema.exceptions.ValidationError'      :'ER_AUT001',
		                  'validator.InvalidPasswordFormatError'        :'ER_AUF002',
		                  'validator.InvalidSecurityNameError'          :'ER_AUF004',
		                  'validator.InvalidUsernameFormatError'        :'ER_AUF004',
		                  'validator.InvalidUsername'                   :'ER_AUT005',
		                  'validator.InvalidToken'                      :'ER_AUT006',
		                  'validator.InvalidAuthentication'             :'ER_AUT007',
		                  'validator.PageNotFoundError'                 :'ER_PNF008', 
		                  'validator.InvalidMethodRequest'              :'ER_IMR009'}


def verify_ErrorReason(ErrorReason):
    for Reason,ErrorCode in Error_Table.Data.items():
        if Reason == ErrorReason:
            return ErrorCode

def get_ErrorCode(ErrorReason):
    for Reason,ErrorCode in Error_Table.Data.items():
        if Reason == ErrorReason:
            return ErrorCode


class InvalidPasswordFormatError(Exception):
	pass

class InvalidUsernameFormatError(Exception):
    pass

class InvalidSecurityNameError(Exception):
    pass

class InvalidUsername(Exception):
    pass

class SQLError(Exception):
    pass

class InvalidToken(Exception):
    pass

class InvalidAuthentication(Exception):
    pass

class InvalidMethodRequest(Exception):
    pass

class PageNotFoundError(Exception):
    pass


def Valid_Security(passw):
    if  re.search(r"[A-Z]", passw):    
        return {"status":"successful","message":'unexpected security name'}
    else:
        raise InvalidSecurityNameError()

def Valid_Username(passw):
    if  re.search(r"[A-Z]", passw) and re.search(r"[a-z]", passw) and re.search(r"\d", passw):    
        return {"status":"successful","message":'password is very strong'}
    else:
        raise InvalidUsernameFormatError()

@app.errorhandler(500)
def check_strogness(passw):
    if re.search(r"[!#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', passw) and re.search(r"[A-Z]", passw) and re.search(r"[a-z]", passw) and re.search(r"\d", passw):    
        return {"status":"successful","message":'Password is Very strong'}
    else:
        raise InvalidPasswordFormatError()